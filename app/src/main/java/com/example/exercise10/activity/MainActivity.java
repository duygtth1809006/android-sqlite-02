package com.example.exercise10.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.exercise10.R;
import com.example.exercise10.database.DBHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etName;
    private EditText etDes;
    private Button btnRegister;
    private Spinner spinner;
    private DBHelper dbHelper;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        dbHelper = new DBHelper(this);
        dbHelper.getReadableDatabase();
    }

    private void initView() {
        etName = findViewById(R.id.etUser);
        etDes = findViewById(R.id.etDes);
        btnRegister = findViewById(R.id.btnRegister);
        checkBox = findViewById(R.id.ck);
        btnRegister.setOnClickListener(this);

        String[] genders = {"Male", "Female", "Unknown"};
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, genders);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v == btnRegister) {
            onRegister();
        }
    }

    private void onRegister() {
        if(etName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter username please", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!checkBox.isChecked()) {
            Toast.makeText(this, "Check rule please", Toast.LENGTH_SHORT).show();
            return;
        }

        String isAdd = dbHelper.addUser(etName.getText().toString(), spinner.getSelectedItem().toString(), etDes.getText().toString());
        Toast.makeText(this, isAdd, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(MainActivity.this, ListUserAct.class);
        startActivity(intent);
    }
}