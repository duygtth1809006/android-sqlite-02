package com.example.exercise10.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.exercise10.R;
import com.example.exercise10.database.DBHelper;

public class ListUserAct extends AppCompatActivity {

    private DBHelper dbHelper;
    private Cursor cursor;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);

        dbHelper = new DBHelper(this);
        ListView lvUser = findViewById(R.id.lvUser);

        cursor = dbHelper.getAllUser();

        adapter = new SimpleCursorAdapter(this, R.layout.activity_item, cursor, new String[] {
                dbHelper.ID, dbHelper.NAME, dbHelper.GENDER}, new int[] {R.id.tvId, R.id.tvName, R.id.tvGender}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        lvUser.setAdapter(adapter);

        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) adapter.getItem(position);
                int _id = cursor.getInt(cursor.getColumnIndex(dbHelper.ID));
                String name = cursor.getString(cursor.getColumnIndex(dbHelper.NAME));
                String gender = cursor.getString(cursor.getColumnIndex(dbHelper.GENDER));
                String des = cursor.getString(cursor.getColumnIndex(dbHelper.DES));

                Intent intent = new Intent(ListUserAct.this, UpdateUserAct.class);
                intent.putExtra(dbHelper.ID, _id);
                intent.putExtra(dbHelper.NAME, name);
                intent.putExtra(dbHelper.GENDER, gender);
                intent.putExtra(dbHelper.DES, des);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        cursor = dbHelper.getAllUser();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();
        dbHelper.close();
    }
}