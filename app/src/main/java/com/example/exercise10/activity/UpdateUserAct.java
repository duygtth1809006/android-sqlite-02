package com.example.exercise10.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.exercise10.R;
import com.example.exercise10.database.DBHelper;

public class UpdateUserAct extends AppCompatActivity implements View.OnClickListener {
    private DBHelper dbHelper;
    private int _id;
    private EditText etName;
    private EditText etGender;
    private EditText etDes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);

        dbHelper = new DBHelper(this);
        initView();

        Intent intent = getIntent();
        _id = intent.getIntExtra(dbHelper.ID, 0);
        String name = intent.getStringExtra(dbHelper.NAME);
        String gender = intent.getStringExtra(dbHelper.GENDER);
        String des = intent.getStringExtra(dbHelper.DES);

        etName.setText(name);
        etGender.setText(gender);
        etDes.setText(des);
    }

    private void initView() {
        etName = findViewById(R.id.etName);
        etGender = findViewById(R.id.etGender);
        etDes = findViewById(R.id.etDes);
        Button btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        Button btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(this);
    }

    private void onUpdate() {
        String isUpdate = dbHelper.updateUser(_id, etName.getText().toString(), etGender.getText().toString() + " update", etDes.getText().toString());
        Toast.makeText(this, isUpdate, Toast.LENGTH_SHORT).show();
        finish();
    }

    private void onDelete() {
        Toast.makeText(this, dbHelper.deleteUser(_id), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpdate:
                onUpdate();
                break;
            case R.id.btnDelete:
                onDelete();
                break;
            default:
                break;
        }
    }
}