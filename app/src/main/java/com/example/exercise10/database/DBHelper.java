package com.example.exercise10.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "USER";
    public static final int DB_VER = 1;

    public static String TABLE_NAME = "TBL_USER";
    public static String ID = "_id";
    public static String NAME = "name";
    public static String GENDER = "gender";
    public static String DES = "des";

    public DBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table " + TABLE_NAME + " ( " +
                ID + " integer primary key, " +
                NAME + " text, " +
                GENDER + " text, " +
                DES + " text )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "drop table if exists " + TABLE_NAME;
        db.execSQL(sql);
        onCreate(db);
    }

    public String addUser(String user, String gender, String des) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME, user);
        values.put(GENDER, gender);
        values.put(DES, des);
        long isSuccess = database.insert(TABLE_NAME, null, values);
        if(isSuccess == -1 ) {
            return "Add Fail";
        } else {
            return "Add Success";
        }
    }

    public String updateUser(int id, String user, String gender, String des) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME, user);
        values.put(GENDER, gender);
        values.put(DES, des);
        long isSuccess = database.update(TABLE_NAME, values,ID + "=?", new String[] {id+""});
        if(isSuccess == -1 ) {
            return "Update Fail";
        } else {
            return "Update Success";
        }
    }

    public String deleteUser(int id) {
        SQLiteDatabase database = this.getReadableDatabase();
        long isSuccess = database.delete(TABLE_NAME,ID + "=?", new String[] {id+""});
        if(isSuccess == -1 ) {
            return "Delete Fail";
        } else {
            return "Delete Success";
        }
    }

    public Cursor getAllUser() {
        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "select * from " + TABLE_NAME;
        Cursor c = database.rawQuery(sql, null);
        return c;
    }
}
